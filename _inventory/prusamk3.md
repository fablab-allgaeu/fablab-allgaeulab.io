---
name: Prusa i3 MKIII
image: assets/prusamk3.jpg
link: https://gitlab.com/fablab-allgaeu/prusa-i3-mk3
description: Prusa i3 MK3 (Metallrahmen) basiert auf den FDM Druckern der RepRap Serie
---
Der Prusa i3 MKIII ist unser neuster 3D Drucker. Er arbeitet mit dem FDM
Verfahren und nutzt einen Direktextruder. Über die magnetisch gehaltene
Federstahlplatte lassen sich die Drucke leicht abnehmen. Zudem druckt er
sehr genau und schnell.

