---
name: Lasercutter
image: assets/lasercutter.jpg
link: https://gitlab.com/fablab-allgaeu/lasercutter
description: Großer 100W CO₂ Laser aus China
---
Unser Lasercutter kann Pappel- und Birkensperrholz, Acrylglas und einige Plastikarten bis 4mm,
Karton bis 2mm, Papier, Folie, Leder und Gummi schneiden und zusätzlich noch eloxiertes
Aluminium und dickere Materialien gravieren. Man kann auch Pizza schneiden, das soll man
aber nicht.