---
name: Käsefräse
image: assets/cnc.jpg
link: #
description: Selbstgebaute 3-Achsen Portalfräse
---
Unsere CNC Fräse ist als Bausatz zu uns gestoßen und befindet sich seither in verschiedenen
Stadien der Funktionstüchtigkeit. Es müssen noch einige Umbauten vorgenommen werden, bevor
wir sie regulär benutzen können. Grundsätzlich lässt sie sich allerdings bednutzen und hat
auch schon einige Objekte hergestellt.