---
title: Blog
layout: default
permalink: /blog/
pagination:
  enabled: true
---

<div class="container">
  <div class="section no-pad-bot">
    <h1>{{ page.title }}</h1>
    <div class="col s12">
      {% for post in paginator.posts %}
        <div class="card {% if post.header_img %}large{% endif %} secondary-color base">
        {% if post.header_img %}
        <div class="card-image">
          <img src="{{ post.header_img | relative_url }}">
          <span class="card-title white-text" style="text-shadow: 1px 1px 5px #000"><h3>{{ post.title }}</h3></span>
        </div>
        {% endif %}
          <div class="card-content white-text">
            {% unless post.header_img %}<span class="card-title"><h3>{{ post.title }}</h3></span>{% endunless %}
            {{ post.excerpt | strip_html | truncate: 250 }}
          </div>
          <div class="card-action">
            <a href="{{post.url |  prepend: site.baseurl }}">Weiterlesen...</a>
            <div class="white-text right">{{ post.date | date: "%b %-d, %Y" }}</div>
          </div>
        </div>
      {% endfor %}
    </div>
  </div>
  <ul class="pagination center">
    <li {% unless paginator.previous_page %} class="disabled" {% endunless %}><a href="{{ paginator.previous_page_path }}"><i class="material-icons">chevron_left</i></a></li>
    {% for trail in paginator.page_trail %}
      <li {% if page.url == trail.path %} class="active" {% else %} class="waves-effect" {% endif %}><a href="{{ trail.path}}">{{ trail.num }}</a></li>
    {% endfor %}
    <li {% unless paginator.next_page %} class="disabled" {% endunless %}><a href="{{ paginator.next_page_path }}"><i class="material-icons">chevron_right</i></a></li>
  </ul>
</div>
<br>
<br>
<br>
<br>
<br>
