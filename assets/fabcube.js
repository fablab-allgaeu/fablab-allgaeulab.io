var camera, scene, renderer, cube, div;

function initcube() {
    div = document.getElementById('3d-logo');
    if (div != null) {
        console.debug("found the element");
        console.debug("width: " + div.clientWidth, ", height: " + div.clientHeight);
        console.debug(div);
    }
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 75, div.clientWidth/div.clientHeight, 0.1, 1000 );

    renderer = new THREE.WebGLRenderer({ alpha: true });
    renderer.setClearColor(div.style.backgroundColor, 0);
    renderer.setSize( div.clientWidth, div.clientHeight );
    div.appendChild(renderer.domElement);

    var material = new THREE.MeshBasicMaterial( { color: div.style.color } );

    var loader = new THREE.GLTFLoader();
    loader.load('/assets/fabcube.glb', function ( gltf ) {
        gltf.scene.traverse(function (child) {
            child.material = material;
            cube = child;
        });
        scene.add(gltf.scene);
    }, undefined, function (error) {
        console.error(error);
    });

    camera.position.z = 3;

    rendercube();
}

function rendercube() {
    requestAnimationFrame( rendercube );

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;

    renderer.render( scene, camera );
}
