---
layout: post
title: Verein
permalink: /verein/
---

Hier findest du alles über den **FabLab Allgäu e.V.**.
###  Geschichte

#### Anfangsjahre

"Technik und Hightech-Fertigungsverfahren für jedermann zugänglich zu machen" ist das Motto der internationalen FabLab-Bewegung. Nach diesem Vorbild engagiert sich das FabLab Allgäu bereits seit über 10 Jahren in Kempten.<br/> 
Die Idee der FabLabs ist es, im Sinne einer offenen Werkstatt, Maschinen der Hochtechnologiefertigung wie 3D-Drucker, Lasercutter, CNC-Fräsen und Elektronikfertigung auch für ungeübte Interessierte benutz- und begreifbar zu machen. 
Bei uns können Mitglieder sowie Gäste Erfahrungen mit den genannten Werkzeugen und Techniken sammeln und sich untereinander austauschen. <br/>
Nicht jeder kann oder will sich zu Hause eine eigene Werkstatt einrichten. Deshalb haben wir uns zusammengeschlossen und in Zusammenarbeit mit der Hochschule Kempten seit 2013 verschiedene Maschinen angeschafft und betreut.

#### Aktuell

Nach 8 Jahren hat sich so aus der anfänglichen Handvoll Studenten nicht nur eine ordentliche Hightech-Werkstatt mit viel Know-How, sondern auch eine bunte Gemeinschaft aus interessierten "Hackern" und Makern allen Alters zusammengefunden, die gemeinsam entwickeln, tüfteln, basteln oder auch nur ein Feierabendbier teilen. Die Begeisterung für Technik treibt uns dabei an, uns stetig weiterzuentwickeln und die Erfahrung mit anderen zu teilen. Bei uns steht der Austausch im Mittelpunkt und wir laden gerne jeden ein, der bei uns seine Projekte realisieren will oder Gleichgesinnte treffen möchte.<br/>
Unsere langjährige Zusammenarbeit mit dem Kempodium e.V. mussten wir Anfang 2020 leider beenden, woraufhin wir am 10.07.2020 einen eigenständigen Verein (FabLab Allgäu e.V.) gegründet haben. Der Aufbau einer neuen Werkstatt wurde uns im Jahr davor durch die Pandemie sehr erschwert, weswegen wir etwas länger auf Raumsuche waren.<br/>
Seit Anfang 2025 dürfen wir dankenswerterweise die Räume des Makerspace Kempten in den Räumen des Jugendhauses mit nutzen.

### Vereinszweck

Zweck des Vereins ist die Förderung der Jugend- und Erwachsenenbildung, Forschung und Wissenschaft sowie des schöpferisch-kritischen Umgangs mit Informations- und Kommunikationstechnologien. <br/>
Verwirklicht wird der Vereinszweck durch den Aufbau und Betrieb einer FabLab genannten nicht-kommerziellen Quartierswerkstatt für eine lokale Eigenproduktion. <br/>
Aufgabe dieses FabLab ist die Bereitstellung einer räumlichen, technischen und personellen Infrastruktur.  Die Besucher des FabLabs sollen angeregt und befähigt werden, zum eigenen und gemeinschaftlichen Nutzen, Kunst- und Designobjekte, Maschinen, Alltagsgegenstände sowie Mechanik-, Elektronik-, Hardware- und Softwarekomponenten selbst zu entwerfen und herzustellen.

### Mitglied werden

Um ein Mitglied im FabLab Allgäu e. V. zu werden, fülle bitte den unten aufgeführten Mitgliedsantrag aus und lasse uns diesen bitte per [Email](mailto:info@fablab-allgaeu.de) zukommen.

Danach erhältst du innerhalb weniger Tage eine Bestätigung von uns.

Gerne kannst du auch Mitglied werden, wenn du noch nicht volljährig bist.

Hierfür bitten wir dich und deine(n) gesetzliche(n) Vertreter(in), den Antrag mit zu unterzeichnen.

Wir freuen uns schon darauf, dich als neues Mitglied willkommen zu heißen! 🤩

### Informationen für Mitglieder

Hier findet ihr eine Auflistung wichtiger Dokumente für den Verein:
<ul class="collection">
  <a class = "collection-item" target="blank" href="{{ '/verein/Gebührenordnung-3ce944feb3537b3ca347eb38619fc33385f9284e.pdf' | relative_url }}">Gebührenordnung</a>
  <a class = "collection-item" target="blank" href="{{ '/verein/mitgliedsantrag.pdf' | relatvie_url }}">Mitgliedsantrag</a>
  <a class = "collection-item" target="blank" href="{{ '/verein/Satzung.pdf' | relative_url }}">Satzung</a>
</ul>

### Kontodaten

```
Kontoinhaber: FabLab Allgäu e.V.
Iban:         DE17 7336 9920 0000 8285 80
BIC:          GENO DEF1 SFO
```
