#!/bin/bash

set -e
set -o pipefail

if [ "$IN_CONTAINER" == "" ]
then

    IMAGE="ruby:3.4"

    docker run -i -t -v $(pwd):/opt \
            -p 35729:35729 \
            -p 4000:4000 \
            --net=host \
            -e IN_CONTAINER=true \
            $IMAGE /opt/start.sh
else

    set -x
    cd /opt
    
    export LC_ALL="C.UTF-8"
    export LANG="en_US.UTF-8"
    export LANGUAGE="en_US.UTF-8"
    
    gem install bundler:2.6
    bundle config set path 'gems'
    bundle install
    bundle exec jekyll build -d /tmp/test 
    bundle exec jekyll serve -d /tmp/test --livereload

fi
