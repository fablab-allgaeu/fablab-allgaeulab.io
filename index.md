---
layout: landing
---

Herzlich Willkommen auf der neuen Homepage des FabLab Allgäu!  
Aktuell befindet sich die Seite noch im Aufbau, der alte Stand ist [hier](http://fablab-allgaeu.de) zu erreichen.

Verbesserungsvorschläge können gerne auf [GitLab](https://gitlab.com/fablab-allgaeu/fablab-allgaeu.gitlab.io/issues/2) eingereicht werden.
