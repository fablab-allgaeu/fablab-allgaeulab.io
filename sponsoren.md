---
layout: post
title: Sponsoren
permalink: /sponsoren/
---

## Danke!
An dieser Stelle möchten wir uns herzlich bei unseren fantastischen Sponsoren bedanken!

Nur dank eurer Hilfe gibt es das FabLab!

Vielen lieben Dank für eure großartige Unterstützung!

## Werdet jetzt Sponsor

Schließt euch jetzt unseren wunderbaren Unterstützern an und engagiert euch für das FabLab Allgäu e.V..

Meldet euch dafür bitte direkt an den Vorstand: [vorstand@fablab-allgaeu.de](mailto:vorstand@fablab-allgaeu.de)


## Sponsoren:

<div class="row">
    {% for sponsor in site.data.sponsoren %}
    <div class="col s12 m6 l4">
      <div class="card white large">
        <div class="card-image" style="top: 50%; transform: translateY(-90%); margin: 0; position: absolute;">
          <img src="{{ sponsor.logo | relative_url }}" style="margin: 5px">
        </div>
        <div class="card-content" style="color: black; position: absolute; bottom: 0;">
          <span class="card-title">
            <a href="{{sponsor.url}}" target="_blank">{{ sponsor.name }}</a>
          </span>
          <p>{{sponsor.text}}</p>
        </div>
      </div>
    </div>
    {% endfor %}
  </div>
            
