---
layout: post
title: Datenschutz
permalink: /datenschutz/
---

Auf dieser Webseite werden keine personenbezogene Daten erhoben, gespeichert oder verabeitet. Es werden keine Cookies verwendet.
Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.

##### OpenStreetMap

Diese Seite nutzt über eine API das Open-Source-Mapping-Werkzeug „OpenStreetMap“ (OSM). Anbieter ist die OpenStreetMap Foundation. Zur Nutzung der Funktionen von OpenStreetMap ist es notwendig, Ihre IP Adresse zu speichern. Diese Informationen werden in der Regel an einen Server von OpenStreetMap übertragen und dort gespeichert. Der Anbieter dieser Seite hat keinen Einfluss auf diese Datenübertragung. Die Nutzung von OpenStreetMap erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote und an einer leichten Auffindbarkeit der von uns auf der Website angegebenen Orte. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar. Mehr Informationen zum Umgang mit Nutzerdaten finden Sie in der [Datenschutzseite von OpenStreetMap](https://wiki.osmfoundation.org/wiki/Privacy_Policy) und hier [http://wiki.openstreetmap.org/wiki/Legal_FAQ](http://wiki.openstreetmap.org/wiki/Legal_FAQ).

##### Verweis auf Dritte
Diese Webseite wird auf [Gitlab](https://gitlab.com/) als [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) gehostet. Die Gitlab Privacy Compliance sind [hier](https://about.gitlab.com/privacy/privacy-compliance/) hinterlegt.
