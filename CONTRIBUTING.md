
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Arbeitsweise](#arbeitsweise)
  - [Änderungen vornehmen](#%C3%A4nderungen-vornehmen)
  - [Änderungen akzeptieren](#%C3%A4nderungen-akzeptieren)
- [Jekyll benutzen](#jekyll-benutzen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Arbeitsweise

### Änderungen vornehmen
Um deine Änderungen auf die Website zu bekommen gibt es ein paar einfache Schritte:
- Mit git einen neuen branch erstellen.
- Auf diesem branch werden deine Änderungen genacht und wie in [Jekyll benutzen](#jekyll-benutzen) ausprobiert.
- Anschließend kann dein branch auf das GitLab Repository gepusht werden und ein Merge Request auf den master erstellt werden.

### Änderungen akzeptieren
Um den Merge Request von jemand anderem zu akzeptieren sind folgende Schritte notwendig:
- Du holst dir den im MR erwähnten branch lokal.
- Wie in [Jekyll benutzen](#jekyll-benutzen) beschrieben schaust du dir die Änderungen lokal an.
- Wenn alles passt, dann kannst du den Merge Button im MR drücken. ACHTUNG: damit wird die Website direkt aktualisiert. Ein paar Sekunden später sind die Änderungen online.


## Jekyll benutzen

Um mit die Website mit Jekyll lokal zu bauen müssen folgende Schritte befolgt werden:

1. Jekyll [installieren](https://jekyllrb.com/docs/installation/)
2. Abhängigkeiten herunterladen: `bundle`
3. Bauen: 
	* Entweder `bundle exec jekyll serve` (mit `--livereload` werden alle Änderungen direkt angezeigt)
	* oder direkt `./start.sh` aufrufen

Und schon sollte die Website lokal verfügbar sein.

Mehr Infos gibts in der [Doku](https://jekyllrb.com/docs/home/) von Jekyll.
