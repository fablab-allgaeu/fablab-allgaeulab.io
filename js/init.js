(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.carousel').carousel();

  }); // end of document ready
})(jQuery); // end of jQuery name space


const OneDayAgo = (date) => {
    const day= 1000 * 60 * 60 * 24;
    const dayago= Date.now() - day;

    return date > dayago;
}

$(document).ready(function(){
  $('.parallax').parallax();
  $('.modal').modal();

  storageKey = 'lastTimeGreetingShown'
  const lastTimeHeaderShownData = localStorage.getItem(storageKey);
  let showModal = true;

  if(lastTimeHeaderShownData) {
    const data = JSON.parse(lastTimeHeaderShownData);
    lastTime = data.lastTimeHeaderShown;
    if(OneDayAgo(lastTime)) {
      showModal = false;
    }
  }

  if(showModal) {
    $('.modal').modal('open');
    const now = new Date();
    const dataToStore = { lastTimeHeaderShown: now.getTime()};
    localStorage.setItem(storageKey, JSON.stringify(dataToStore));
  }

});


var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
	osmAttrib = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	osm = L.tileLayer(osmUrl, { attribution: osmAttrib });

var labLocation = [47.7295155, 10.3121528];
var zoomLevel = 13;

var map = L.map('map', { zoom: zoomLevel, dragging: !L.Browser.mobile, scrollWheelZoom: false }).setView(labLocation, zoomLevel).addLayer(osm);

L.marker(labLocation)
	.addTo(map)
	.bindPopup('FabLab Allgäu')
	.openPopup();
