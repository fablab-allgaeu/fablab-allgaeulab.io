---
layout: default
title: Ausstattung
permalink: /ausstattung/
---

<div class="container">
  <h1>Ausstattung</h1>
  <div class="row">
    {% for item in site.inventory %}
      <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="{{ item.image | relative_url }}">
          </div>
          <div class="card-content">
            <span class="card-title activator white-text">{{ item.name }}<i class="material-icons right">more_vert</i></span>
            <div class="white-text" style="height: 2em;">{{ item.description }}</div>
          </div>
          <div class="card-reveal">
            <span class="card-title white-text">{{ item.name }}<i class="material-icons right">close</i></span>
            <div class="white-text">{{ item.content }}</div>
          </div>
          <div class="card-action" style="border-color: white;">
            <a target="_blank" href="{{ item.link }}" style="margin-right: 0"><div class="valign-wrapper">Mehr<i class="tiny material-icons">launch</i></div></a>
          </div>
        </div>
      </div>
    {% endfor %}
  </div>
</div> 
