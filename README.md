![Build Status](https://gitlab.com/fablab-allgaeu/fablab-allgaeu.gitlab.io/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [About](#about)
- [Mitmachen](#mitmachen)
- [Online stellen der Homepage](#online-stellen-der-homepage)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About

Website Projekt für http://fablab-allgaeu.gitlab.io basierend auf der Beispiel [Jekyll](https://gitlab.com/pages/jekyll) Website von GitLab Pages.

## Mitmachen

Neue Einträge oder Verbesserungen an dieser Seite sind gerne gesehen. Am Besten hältst du dich an die Beschreibungen auf [CONTRIBUTING.md](CONTRIBUTING.md).

## Online stellen der Homepage
Die Homepage wird wie in [`.gitlab-ci.yml`](.gitlab-ci.yml) definiert automatisch vom master branch gebaut und live gestellt. Wir müssen uns um nichts mehr kümmern.
