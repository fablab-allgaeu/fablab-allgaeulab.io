---
layout: post
title:  "Elektronik Basar"
date:   2015-04-21
---
Am kommenden Samstag findet ein Elektronik Basar im Kempodium statt. Verkauft werden gebrauchte Elektronikgeräte, Zubehör und Ersatzteile. Der Basar findet von 10 - 14 Uhr statt. Ihr seid herzlich eingeladen vorbeizuschauen.

Schöne Grüße

Euer FabLab

