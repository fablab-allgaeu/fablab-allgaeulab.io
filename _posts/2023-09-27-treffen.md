---
layout: post
title: 💻 Monatliches Treffen des FabLab Allgäu e.V.
date:   2023-09-25
author: Jan
categories: Verein
---
Liebe Mitglieder und Unterstützer des Fablab Allgäu e.V.,


wir laden euch herzlich zu unserem monatlichen Treffen am Mittwoch, den 27. September, um 20:00 Uhr im Stadtjugendring Kempten, Bäckerstraße 9, ein.


Wir werden Updates zu laufenden Projekten präsentieren, kommende Veranstaltungen besprechen und Raum für offene Diskussionen bieten. Eure Ideen und Anregungen sind herzlich willkommen.

Wir freuen uns auf eure Teilnahme!


Mit herzlichen Grüßen,


Die Vorstandschaft des Fablab Allgäu e.V.
