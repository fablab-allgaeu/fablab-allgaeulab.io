---
layout: post
title:  "Neue Website ist live"
date:   2019-11-13
author: Bene
categories: update
---
Umzug der Homepage auf GitLab Pages ist "fertig".

Wenn du diesen Post hier liest weißt du es ja schon:
Unsere [Website](http://www.fablab-allgaeu.de/) haben wir auf GitLab Pages umgezogen. 

So eine Website ist natürlich immer im Umbruch, wenn du also Vorschläge oder Anregungen hast, beteilige dich einfach auf [GitLab](https://gitlab.com/fablab-allgaeu/fablab-allgaeu.gitlab.io).


