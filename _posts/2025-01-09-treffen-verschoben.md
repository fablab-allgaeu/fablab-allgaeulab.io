---
layout: post
title: ⚠️  Monatliche Treffen verschoben
date:   2025-01-09
author: Jan
categories: Verein
---
Liebe Mitglieder und Freunde des FabLab Allgäu,

leider muss ich das monatliche Treffen nächste Woche verschieben.

Sobald der Ausweichtermin feststeht, informiere ich euch wie gewohnt via Telegram und der Mailingliste.

Mit freundlichen Grüßen

Jan (1. Vorstand)
