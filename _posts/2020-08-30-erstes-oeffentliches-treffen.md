---
layout: post
title:  "Erstes öffentliches Treffen des FabLab Allgäu e. V."
date:   2020-08-30
author: Jan
categories: Verein Treffen
---

Liebe Freunde des FabLab Allgäu e. V.,

<br>
am Mittwoch den 02.09.2020 werden wir um 19 Uhr unser erstes öffentliches Treffen im [Künstlerhaus in Kempten](https://www.openstreetmap.org/?mlat=47.72436&mlon=10.31537#map=19/47.72436/10.31537&layers=N) veranstalten.

Dazu sind alle Interessierten herzlich eingeladen. 

Wir werden die Gelegenheit nutzen um über den aktuellen Stand der Vereinsgründung zu informieren und über die weiteren Schritte zu diskutieren.

Damit wir genügend Plätze im Künstler reservieren würden wir um eine Anmeldung bitten. Daher entweder an der Telegramm Umfrage teilzunehmen, in Telegramm eine DM zu schreiben oder uns kurz via Email kontaktieren.

<br>
Die Vorstandschaft freut sich auf euer Kommen!


<br>
<br>
<br>
<br>
Anfahrt Künstlerhaus Kempten:
<div id="mapKuenstler" class="map" style="height: 380px;"></div>
<script>
var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
		osmAttrib = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
		osm = L.tileLayer(osmUrl, {attribution: osmAttrib});

	var mapKuenstler = L.map('mapKuenstler', {zoom: 16, dragging: !L.Browser.mobile, scrollWheelZoom: false}).setView([47.72436, 10.31537], 16).addLayer(osm);

	L.marker([47.72436, 10.31537])
		.addTo(mapKuenstler)
		.bindPopup('Künstlerhaus Kempten<br>Beethovenstraße 2<br> 87435 Kempten')
		.openPopup();
</script>
