---
layout: post
title:  "Finalteilnehmer im AÜW VereinsEnergie Wettbewerb"
date:   2021-05-05
author: Bene
categories: Verein VereinsEnergie
---

Dank deiner Hilfe sind wir im Finale vom [Wettbewerb AÜW VereinsEnergie](https://auew-vereinsenergie.de/). 

Vielen Dank dafür 🥳👍.

<b>Update:</b> Das Finale ist beendet.
<br/><br/>

Ab jetzt geht es bis Donnerstag 16:00 um die Wurst (bzw. ums Geld). Je mehr Personen [für uns abstimmen](https://auew-vereinsenergie.de/projekte/6073e20075313e185ad28fd7), desto höher fällt unser Preisgeld aus.

<br/>

Bitte stimm noch einmal mit deiner Email Adresse für uns ab. Das Preisgeld können wir gut für den Aufbau unserer offenen Werkstatt gebrauchen. 

<br/>

Wie immer halten wir dich natürlich auf dem Laufenden. Hier im Blog oder natürlich auf [Telegram](https://t.me/joinchat/CvEbEA2WqAEv1-AhV7Hp9g), [Facebook](https://www.facebook.com/fablaballgaeu), [Instagram](https://www.instagram.com/fablaballgaeu/), und der [Mailingliste](http://lists.fablab-allgaeu.de/cgi-bin/mailman/listinfo).

