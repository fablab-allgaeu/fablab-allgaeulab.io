---
layout: post
title: 🎅 Weihnachtsgrüße
date:   2024-12-24
author: Jan
categories: Verein
---

Liebe Fablab Mitglieder und Freunde,

das Jahr 2024 geht zu Ende, und auch wenn wir dieses Jahr ohne eigene Werkstatt auskommen mussten, hat uns das nicht davon abgehalten, als Gemeinschaft eine großartige Zeit zusammen zu verbringen. Besonders die Treffen der letzten Monate waren außergewöhnlich schön - ein herzliches Dankschön dafür im Namen der gesamten Vorstandschaft. Die hohe Anzahl an neuen Teilnehmern zeigt eindrucksvoll, dass die Begeisterung und Nachfrage für das Fablab ungebrochen ist – selbst ohne festen Standort.

**Umso mehr freuen wir uns, dass wir Anfang nächsten Jahres endlich unsere neue Werkstatt beziehen können!** Gemeinsam werden wir diesen neuen Raum mit Ideen, Projekten und Erfindergeist füllen. Die Vorfreude darauf, endlich wieder eine eigene Werkstatt zu haben, ist riesig, und wir können es kaum erwarten, mit euch loszulegen.

Bis dahin wünschen wir euch frohe Weihnachten und einen guten Rutsch ins neue Jahr. Genießt die Zeit mit euren Liebsten und ladet eure kreativen Akkus auf, um schon die eine oder andere Idee für kommende Projekte zu spinnen.

**2025 wird unser Jahr** – mit neuer Werkstatt, fantastischen Leuten und gewohntem Tatendrang! Danke, dass ihr Teil dieser besonderen Community seid.

Euer Vorstand des Fablab Allgäu e.V.
