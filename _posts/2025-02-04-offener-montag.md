---
layout: post
title: 🪆 Nach dem ersten Treffen ist vor dem Treffen
date:   2025-02-04
author: Jan
categories: Verein
---

##### Liebe Mitglieder und Freunde des FabLab Allgäu,

Was für ein grandioser Abend! Ein riesiges Dankeschön an alle, die gestern zur Einweihung unserer neuen Werkstatt gekommen sind.

Nun geht es richtig los – mit neuen Räumen, neuen Maschinen und neuen Ideen! Hier eine kurze Zusammenfassung der wichtigsten Neuerungen:

##### Offener Montag – Unser neuer Treffpunkt!
Wir treffen uns jetzt **jeden Montag ab 18 Uhr im Jugendhaus** (Landwehrstraße 2, 87439 Kempten) zum offenen Montag. Egal, ob du an einem Projekt arbeiten oder dich mit anderen austauschen willst – komm vorbei! Die Klingel ist leider nicht besonders laut. Falls ihr also vor verschlossener Tür steht, schreibt einfach in die Telegram-Gruppe.

##### Neues aus der Werkstatt

- **3D-Drucker:** Unsere **fünf Prusa 3D-Drucker** sind bereits einsatzbereit. Bringt einfach euer eigenes Filament mit, und los geht’s!
- **Bambu Lab 3D-Drucker:** Wir gehen davon aus, dass wir ihn in ein bis zwei Wochen startklar haben.
- **Lasercutter & CNC-Fräse:** Hierfür planen wir in den kommenden Wochen eine offizielle Einweisung. Stay tuned!
- **Siebdruckmaschine:** Da das Interesse daran aktuell eher gering ist, hat dieses Thema momentan keine Priorität.

Aktuell suchen wir **Freiwillige, welche die neuen Maschinen auf unserer Webseite** ([Link zur Ausstattung](/ausstattung)) dokumentieren. Wenn du Lust hast, dich einzubringen, melde dich gerne!

##### Mitglied werden

Du möchtest gerne Mitglied werden und bei uns mitwirken?
Dann bringe am besten gleich den ausgefüllten [Mitgliedsantrag](/verein/mitgliedsantrag.pdf) am Montag mit.

Wir freuen uns auf viele kreative Projekte in unserer neuen Werkstatt. Bis bald beim offenen Montag!

Eure Vorstandschaft

Jan, Matthias & André

