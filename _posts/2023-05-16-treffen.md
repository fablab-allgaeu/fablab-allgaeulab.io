---
layout: post
title:  "Eine erfrischende Abwechslung: Monatliches Treffen beim Cordella"
date:   2023-05-15
author: Jan
categories: Verein
---

Liebe Mitglieder und Freunde des FabLab Allgäu e.V.,

es ist wieder soweit! Unser monatliches Treffen steht vor der Tür und diesmal haben wir eine kleine, aber erfrischende Änderung geplant. Statt uns wie üblich beim Stadtjugendring zu treffen, treffen wir Sie diesmal zu einer gemütlichen Runde Eisessen beim Cordella in Kempten ein.

Wir treffen uns am Mittwoch, den 17. Mai, um 20 Uhr. Im Gegensatz zu unseren üblichen Treffen, wo wir oft an spannenden Projekten arbeiten und neue Ideen generieren, wird diesmal das Hauptthema ein entspannter Austausch über die kommende Jahreshauptversammlung und das Sommerfest sein.

Die Jahreshauptversammlung und das Sommerfest sind immer Highlights in unserem Vereinsjahr und wir freuen uns darauf, diese wichtigen Veranstaltungen mit euch zu besprechen und zu planen. Wir sind davon überzeugt, dass bei einer Kugel Eis (oder fünf) die besten Ideen entstehen können!

Egal, ob du ein langjähriges Mitglied bist, neu im Verein bist oder einfach nur Interesse an dem, was wir tun, hast: Du bist herzlich eingeladen. Unsere Treffen sind immer eine tolle Gelegenheit, andere Mitglieder kennenzulernen, mehr über aktuelle Projekte zu erfahren und natürlich, um sich aktiv einzubringen.

Wir freuen uns darauf, dich bei Cordella zu sehen und gemeinsam einen entspannten Abend zu verbringen.

Bis nächste Woche,

Jan (1. Vorstand)
