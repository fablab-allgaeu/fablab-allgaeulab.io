---
layout: post
title:  "Teilnahme am AÜW VereinsEnergie Wettbewerb"
date:   2021-04-12
author: Jan
categories: Verein VereinsEnergie
---

Wir nehmen nun offiziell am [Wettbewerb AÜW VereinsEnergie](https://auew-vereinsenergie.de/) teil und ~~sollten
demnächst~~ *sind jetzt* mit vielen weiteren Vereinen [auf der Seite der Teilnehmer](https://auew-vereinsenergie.de/projekte/)
aufgelistet. 🥳
<br/>
[Hier](https://auew-vereinsenergie.de/projekte/6073e20075313e185ad28fd7) könnt ihr ab Dienstag direkt für uns abstimmen.

<b>Update:</b> Die Abstimmung ist beendet.
<br/><br/>

#### Der Wettbewerb 🏆
Bei diesem Wettbewerb können 12 Vereine im Einzugsgebiet des AÜW Preise von 250€ bis 2000€ 💵 gewinnen.


Aktuell (Stand Montag, 12.04.2021) sind dort 29 Vereine gelistet, daher stehen die
Gewinnchancen für jeden Verein sehr gut.

#### Warum möchten wir gewinnen? 🍀
Für den Aufbau unserer offenen Werkstatt benötigen wir ein gewisses Grundkapital
um die elementare Ausstattung wie Stühle 🪑, Wandfarbe 🎨, Lötkolben 🔧 oder
Ähnliches 🦄 zu finanzieren.

Auch werden wir die extra Kosten für das Corona Hygienie Konzept 😷 der Werkstatt stemmen müssen.

#### Phase Eins☝️: Qualifikation
Der Wettbewerb ist in zwei Phasen ✌️ unterteilt.

In der ersten Phase qualifizieren sich die Vereine für das Finale.
Dabei kann jede Person 🙋[auf der Seite des AÜW VereinsEnergie](https://auew-vereinsenergie.de/projekte/)
für seinen Verein **täglich** abstimmen.

Diese Phase dauert 3 Wochen und startet am Dienstag den **13. April 2021 um 10 Uhr** 🕙
und endet am Dienstag den **4. Mai um 10 Uhr** 🕙.

Als Erinnerung zum Abstimmen stellen wir dir [hier einen Kalender im ical Format 📆]({{"/assets/Fablab_Allgaeu-AUEW_VereinsEnergie.ics" | url_absolut
}}) zur Verfügung.

Die 12 Vereine mit der höchsten Stimmzahl kommen in das Finale.

#### Phase Zwei✌️: Finale
Nach der Qualifikation kann jeder für zwei Tage, also bis zum 6. Mai 16 Uhr 🕓,
mit einer Email Adresse 📧 für uns stimmen.

Das Fablab braucht dich, daher lasse bitte deine Familie 👪, Freunde 🦸, Arbeitskollegen 👷,
Nachbarn 💃 und andere humanoide Lebensformen 👽 für das Fablab abzustimmen.

Die Gewinne des Wettbewerbs 🏆 sind wie folgt gestaffelt:

| Platz   | Gewinn  |
|---------|---------|
| 1. 🥇	  | 2.000 € |
| 2. 🥈	  | 1.500 € |
| 3. 🥉	  | 1.000 € |
| 4. - 5. | 750 €   |
| 6. - 8. | 500 €   |
| 9. - 12.| 250 €   |

#### Die Preise für kreatives Engagement 🎨
Zustätzlich zu dem Wettbewerb, gibt es noch 6 Preise von je 250€ für kreatives Engagement.

Leider gibt es zu diesen Preisen keine weiteren Angaben.

#### Bitte spielt fair! 🎮
Natürlich ist uns klar, dass wir alle technikbegeisterte Hacker 👨‍💻  sind und jeder sofort
sich überlegt, wie wir den Wettbewerb hacken können.

Es treten viele Vereine an, welche es absolut verdient haben zu gewinnen.

Gefälschte Stimmen werden manuell aus dem Ergebnis gefiltert und führen
im schlimmsten Fall zum Ausschluss aus dem Wettbewerb 😭.

Bleibt daher bitte fair und versucht nicht den Wettbewerb zu manipulieren!👍

#### Danke AÜW! ❤️
An dieser Stelle möchten wir uns herzlich bei dem AÜW dafür bedanken, dass die lokalen
Vereine in dieser schweren Zeit unterstützt werden.

Vielen Dank für diese großartige Idee! 😘

#### Bleibt auf dem Laufenden 💌
Wir werden selbstverständlich hier auf der Seite und wie gewohnt in unserer
Telegramgruppe bzw. Mailingliste über den aktuellen Stand des Wettbewerbs berichten.
