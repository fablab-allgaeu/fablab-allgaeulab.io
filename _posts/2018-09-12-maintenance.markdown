---
layout: post
title:  "Überarbeitung der Website"
date:   2018-09-12
author: Bene
categories: update
---
Umzug der Homepage auf GitLab Pages.

Wir sind gerade dabei unsere [Website](http://www.fablab-allgaeu.de/) langsam auf GitLab Pages umzuziehen. 
Aktuell ist das hier nur eine Vorschau. Wenn du Vorschläge oder Anregungen hast, oder einfach beim Umzug helfen willst, beteilige dich einfach auf [GitLab](https://gitlab.com/fablab-allgaeu/fablab-allgaeu.gitlab.io).


