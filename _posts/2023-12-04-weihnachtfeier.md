---
layout: post
title: 🎄 Weihnachtsfeier
date:   2023-12-04
author: Jan
categories: Verein
---
Liebe Mitglieder und Freunde des FabLab Allgäu e.V.,


Weihnachten steht vor der Tür und es ist wieder Zeit für unsere jährliche Weihnachtsfeier! Wir laden euch herzlich ein, gemeinsam mit uns das Jahr 2023 ausklingen zu lassen.


**Datum:** Montag, 11. Dezember 2023


**Uhrzeit:** Ab 19 Uhr


**Ort:** Stadtjugendring Kempten, Bäckerstraße 9


Es erwartet euch eine Feier mit köstlichen Snacks und Glühwein.


Um uns die Planung zu erleichtern, wäre eine Voranmeldung hilfreich, damit wir genügend Snacks und Getränke für alle bereithalten können.


Wir freuen uns auf euer zahlreiches Erscheinen!


Eure Vorstandschaft des FabLab Allgäu e.V.
