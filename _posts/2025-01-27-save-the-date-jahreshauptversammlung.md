---
layout: post
title: 🦫 Save the Date - Jahreshaupversammlung 2025
date:   2025-01-27
author: Jan
categories: Verein
---
Liebe Mitglieder, Freunde und Interessierte des Fablab Allgäu e.V.,

es ist wieder soweit: Unsere Jahreshauptversammlung steht bevor! Bitte markiert euch den Abend des **24. Februar 2025** schon jetzt dick im Kalender.

Die genauen Uhrzeiten, der Veranstaltungsort sowie die Tagesordnungspunkte werden wir in den kommenden Wochen bekannt geben. Haltet also die Augen offen für weitere Informationen per E-Mail, in unserem Blog und in Telegram.

Wir freuen uns auf zahlreiches Erscheinen und einen regen Austausch mit euch allen!

Euere Vorstandschaft
