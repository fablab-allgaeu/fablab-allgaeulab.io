---
layout: post
title:  🦣 Monatliches Treffen des FabLab Allgäu e.V.
date:   2023-08-11
author: Jan
categories: Verein
---
Liebe Mitglieder und Freunde des Fablab Allgäu e.V.,


wir laden euch herzlich zu unserem bevorstehenden monatlichen Treffen ein, bei dem wir gemeinsam über unsere Vision der Innovation und Kreativität im Allgäu sprechen werden.


**Datum und Uhrzeit**: Mittwoch 16. August, 20:00 Uhr


**Ort**: Stadtjugendring Kempten, Bäckerstraße 9


Ein zentrales Thema wird das Gespräch mit der Stadt Kempten von letzter Woche sein.
Wir haben bereits wichtige Schritte unternommen, um unsere Ideen zur Förderung von Technologie und Kreativität zu teilen.
Dieses Treffen bietet die Gelegenheit, den Rückblick auf dieses Gespräch zu diskutieren und zukünftige Schritte zu planen.


Zusätzlich werden wir aktuelle Projektupdates teilen, kommende Veranstaltungen besprechen und Raum für offene Diskussionen bieten.
Eure Teilnahme und eure Ideen sind entscheidend, um unsere Gemeinschaft weiterzuentwickeln.


Wir freuen uns auf eure Teilnahme.


Eure Fablab Allgäu e.V. Vorstandschaft

