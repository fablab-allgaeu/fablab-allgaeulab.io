---
layout: post
title:  "Frohe Weihnachten"
date:   2015-12-21
---
Liebe Freunde des Fablabs,

wir wünschen euch eine besinnliche Weihnachtszeit und einen guten Rutsch ins neue Jahr. Auch wir gehen nun in die Weihnachtsferien und sind ab dem 6. Januar 2016 wieder für euch da.

Bis dahin,

euer Fablab Allgäu
