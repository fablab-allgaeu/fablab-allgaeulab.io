---
layout: post
title:  "🐧 FabLab Allgäu beim LinuxInfo Tag in Augsburg – Seid dabei!"
date:   2023-04-28
author: Jan
categories: Linux
---

Liebe FabLab Allgäu-Freunde und Technikbegeisterte,

wir freuen uns, euch mitteilen zu dürfen, dass unser FabLab Allgäu Team am kommenden Samstag, den 28. April 2023, am [LinuxInfo Tag an der Hochschule Augsburg](https://www.luga.de/static/LIT-2023/) teilnehmen wird. Dieses Event bietet eine hervorragende Gelegenheit, um sich über die neuesten Entwicklungen und Trends in der Open-Source-Welt zu informieren und sich mit Gleichgesinnten auszutauschen.

Der LinuxInfo Tag ist eine jährlich stattfindende Veranstaltung, die sich der Verbreitung von Freier Software und Open Source-Technologien widmet. Die Veranstaltung bietet ein breites Spektrum an Vorträgen, Workshops und Diskussionsrunden, die von Experten aus der Branche geleitet werden. Themen wie Linux-Distributionen, Anwendungen, Programmierung und Netzwerktechnik werden ausführlich behandelt und bieten eine fundierte Wissensbasis für Anfänger und Fortgeschrittene gleichermaßen.

Wir laden alle Mitglieder und Interessierte herzlich ein, uns auf dieser spannenden Reise zu begleiten und gemeinsam den LinuxInfo Tag in Augsburg zu besuchen. Ob ihr bereits Linux-Profis seid oder euch einfach für Technik und Open Source begeistert, hier ist für jeden etwas dabei.

Treffpunkt für die gemeinsame Fahrt nach Augsburg wird der Bahnhof Kempten sein. Die genaue Uhrzeit und weitere Informationen werden via Telegram geteilt. Bitte meldet euch bei Interesse in unserer Telegramgruppe, damit wir entsprechend planen können.

Wir freuen uns auf einen informativen und inspirierenden Tag in Augsburg und hoffen, dass ihr zahlreich mit uns teilnehmt. Lasst uns gemeinsam in die faszinierende Welt von Linux und Open Source eintauchen und die Kraft der freien Software weiterverbreiten!

Bis bald,
Jan
