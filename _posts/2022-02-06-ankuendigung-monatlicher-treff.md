---
layout: post
title:  "📣 Ankündigung: Monatlicher Treff"
date:   2022-02-06
author: Jan
categories: Verein
---

Wie im letzten Treffen vereinbart, werden wir uns ab sofort **jeden dritten Mittwoch im Monat um 20 Uhr** treffen.

Der nächste Termin ist somit Mittwoch der 16. Februar um 20 Uhr.

Genauere Infos dazu werde ich euch kurz vor dem Termin über [unsere Telegram Gruppe](https://t.me/joinchat/CvEbEA2WqAEv1-AhV7Hp9g) zukommen lassen. 
