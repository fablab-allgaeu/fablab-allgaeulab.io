---
layout: post
title: 🚀 Neues Zuhause für das FabLab Allgäu – Kommt vorbei!
date:   2025-01-29
author: Jan
categories: Verein
---
Liebe Mitglieder und Freunde des FabLab Allgäu,

wir haben großartige Neuigkeiten: **Wir haben endlich wieder eine Werkstatt!**

Diese ist mit 3D-Druckern, Lasercutter und vielen weiteren Maschinen bestens ausgestattet.

Zum ersten Termin in der neuen Werkstatt seid ihr alle herzlich eingeladen – kommt vorbei, schaut euch die Räume an und feiert mit uns den Neuanfang!

**Wo:** Jugendhaus (1. OG), Landwehrstraße 2, 87439 Kempten (direkt am Hofgarten)

**Wann:** Montag, 03. Februar 2025, von 16:30 Uhr bis 22 Uhr

Wir freuen uns rießig, wieder einen Ort zu haben, an dem wir gemeinsam unsere Projekte verwirklichen und uns austauschen können! Hierfür danken wir der Stadt Kempten, inbesondere dem Jugendhaus und allen anderen Unterstützern, die das möglich gemacht haben.

Eure Vorstandschaft

Jan, Matthias & André
