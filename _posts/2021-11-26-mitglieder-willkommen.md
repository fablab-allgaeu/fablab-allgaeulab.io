---
layout: post
title:  "👋 Neue Mitglieder herzlich willkommen!"
date:   2021-11-26
author: Jan
categories: Verein
---

Jetzt ist es soweit! Unser Verein öffnet sich endlich neuen Mitgliedern.

Ab sofort kannst du Mitglied unseres FabLab Allgäu e. V. werden.

Nähere Informationen zu dem Mitgliedsantrag findest du unter dem [Menupunkt "Verein" im Abschnitt "Mitglied werden"]({{ "/verein#mitglied-werden" | relative_url }}).

Sollten euch Fehler auffallen, so meldet diese bitte wie gewohnt in unserer Telegramm Gruppe oder per Email.

Wir freuen uns jetzt schon, dich als Mitglied im FabLab Allgäu e. V. begrüßen zu dürfen! 🤩
