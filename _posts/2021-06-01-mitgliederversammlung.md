---
layout: post
title:  "Mitgliederversammlung 2021"
date:   2021-06-01
author: Jan
categories: Verein
---

## Mitgliederversammlung 2021

Soeben habe ich an alle Mitglieder des FabLab Allgäu e. V.
die offizielle Einladung zur diesjährigen Mitgliederversammlung
am 16. Juni 2021 verschickt.

Bitte denkt daran, dass Anträge zur Tagesordnung eine Woche vor
Versammlungsbeginn beim Vorstand, schriftlich mit kurzer Begründung,
einzureichen sind.

Die Vorstandschaft würde sich freuen, wenn wir dich auf der
Mitgliederversammlung begrüßen dürfen! 
