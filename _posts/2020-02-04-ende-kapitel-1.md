---
layout: post
title:  "Ende Kapitel 1 - wir sind nicht mehr Teil des Kempodiums"
date:   2020-02-04
author: Bene
categories: openlab
---
Die Vorstandschaft des Kempodiums hat uns am 21.01.2020 darüber informiert, dass alle persönlichen Gegenstände, die sich im FabLab befinden, bis zum 31.01.2020 entfernt werden müssen. 

Seit Oktober 2019 hat es sich schon abgezeichnet, dass das FabLab nicht mehr lange im Kempodium verweilen wird. Dass es jetzt dann doch so schnell geht haben wir allerdings nicht erwartet. 
Wir finden es sehr schade, dass wir hier vor vollendete Tatsachen gestellt wurden, denn die letzten 6 Jahre waren zum Großteil doch sehr schön. Danke an Alle die uns und allen unseren Besuchern das ermöglicht haben!

In den letzten 2 Wochen haben wir dann mit der tatkräftigen Unterstützung der Hochschule nun die Zelte abgebrochen und alles geräumt. Somit wird es das FabLab in seiner bisherigen Form nicht mehr geben. 

Wir wollen auch in Zukunft in einem FabLab Allgäu unsere Projektgruppen, Workshops und das OpenLab anbieten. Details dazu können wir leider noch nicht nennen, denn teilweise kennen wir sie selbst noch nicht.

Wir melden uns wieder, wenn wir genau wissen wie es weiter geht. Auf der Mailingliste oder in der Telegram Gruppe gibt es die aktuellsten Infos.
