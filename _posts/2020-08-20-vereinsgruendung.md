---
layout: post
title:  "Das FabLab Allgäu ist tot - lang lebe der FabLab Allgäu e. V."
date:   2020-08-20
author: Bene
categories: openlab verein
---
Zuallererst die guten Nachrichten:
Irgendwie geht es schon weiter, ein erster Meilenstein dazu ist erreicht.
Am 10. Juli haben wir uns getroffen und einen Verein gegründet, der seit dem 30.07.2020 als **FabLab Allgäu e. V.** eingetragen ist.

Die Gründung eines Vereins hatten wir schon Anfang des Jahres geplant. Durch Corona und den Lockdown hat sich das dann aber doch bis jetzt gezogen.
Aber das ist auch nicht so schlimm, ein OpenLab hätten wir in dieser Zeit eh nicht anbieten können.

## Wie geht es jetzt weiter?

### Räumlichkeiten

Aktuell haben wir keine Räumlichkeiten. Als Verein sind wir jetzt aber schon tatkräftig auf Raumsuche.

### OpenLab

Das Öffnen eines neuen OpenLabs hängt zuallererst natürlich von den Räumlichkeiten ab. Aber auch wenn welche gefunden und bezogen sind, müssen wir natürlich schauen, wie wir es mit einem guten Hygienekonzept öffnen können.
Das wird somit also noch etwas dauern.

### Mitgliedschaft im Verein

Wir arbeiten gerade noch an ein paar Formalitäten. Wenn die geklärt sind dann können wir auch weitere Personen als Mitglieder des Vereins aufnehmen. Hoffentlich unterstützt ihr uns dann dabei, wenn es soweit ist.

 

Wirklich viel Info ist das jetzt leider nicht, aber wir wollten allen Interessierten mal ein Lebenszeichen schicken. Es geht voran! Sobald es wieder etwas konkretes gibt, informieren wir euch natürlich darüber. In der Telegram Gruppe sind die News am aktuellsten, wichtiges geht wie immer auch über die Mailingliste. Beides findet ihr unten verlinkt.
