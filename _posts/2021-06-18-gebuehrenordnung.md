---
layout: post
title:  "Gebührenordnung verabschiedet 🎉"
date:   2021-06-18
author: Jan
categories: Verein
---

Auf unserer Mitgliederversammlung am vergangenen Mittwoch verabschiedeten
wir unsere erste Gebührenordnung. 🎉

Diese findet ihr nun unter [dem Menupunkt "Verein"]({{ "/verein#informationen-für-mitglieder" | relative_url }})
bei den `Informationen für Mitglieder` oder
[direkt hier](/verein/Gebührenordnung-3ce944feb3537b3ca347eb38619fc33385f9284e.pdf).
