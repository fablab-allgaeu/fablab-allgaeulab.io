---
layout: post
title:  "Kommende OpenLab - Termine"
date:   2015-04-07
---
Am 11. April findet das nächste OpenLab statt. Von 15 bis 18 Uhr sind wir parallel zum Repair Cafe für euch da.

Am 9. Mai findet ein OpenLab  speziell im Rahmen der Aktionstage der offenen Werkstätten statt. Aus diesem Anlass gibt es an diesem Tag auch einen kleinen Lötkurs, bei dem man Entscheidungsfinder basteln kann und
es besteht die Möglichkeit unsere 3D-Doodler auszuprobieren.

Weitere Infors zu den Aktionstagen findet ihr unter folgendem Link:
[http://www.offene-werkstaetten.org/seite/aktionstage-2015](http://www.offene-werkstaetten.org/seite/aktionstage-2015)

