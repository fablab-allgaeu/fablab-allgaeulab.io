---
layout: post
title: 👀 Monatliches Treffen des FabLab Allgäu e.V.
date:   2024-09-15
author: Jan
categories: Verein
---
**Liebe Mitglieder und Freunde des Fablab Allgäu e.V.,**

ich lade euch herzlich zu unserem monatlichen Treffen im September ein.

Das Treffen findet am Mittwoch, den 18. September, um 20:00 Uhr statt.

Die genaue Location wird in den kommenden Tagen bekannt gegeben, wir halten euch dazu auf dem Laufenden.

**Wir freuen uns auf eure Teilnahme!**

Mit freundlichen Grüßen

Jan (1. Vorstand)
