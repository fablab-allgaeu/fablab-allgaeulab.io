---
layout: post
title:  "Linux, Mate und Pizza"
date:   2017-11-27
---
Linux ist ein Betriebssystem und eine freie und sichere Alternative zu Microsoft Windows. Als Open Source Software wird Linux nicht von einer einzelnen Firma entwickelt, sondern von Freiwilligen aus der ganzen Welt. Auch das Betriebssystem Android basiert auf Linux. Da Linux auch auf den meisten Servern läuft, ist Linux somit eines der am weitesten verbreiteten Betriebssysteme auf der Welt, obwohl es viele Menschen gar nicht kennen. Linux für den Desktop kommt mit einer umfangreichen und freien Sammlung an Programmen für Internet, Büro, Multimedia und Bildung und läuft auch auf älteren Rechnern ganz flott. 

Wenn du Linux kennen lernen und es auf deinem Laptop ausprobieren möchtest dann komm zu unserem Workshop. Bitte bringe deinen Laptop mit, wenn du Linux auf deinem eigenen Gerät ausprobieren möchtest. Wenn du Linux auf deinem Notebook installieren möchtest, denke bitte daran, dass dabei alle Daten gelöscht werden.

Der Eintritt zu der Veranstaltung beträgt 15 Euro pro Person. Getränke und Essen, sind nicht im Preis enthalten.

Termin: Mittwoch, 6. Dezember von 18 – 21 Uhr im Fablab Allgäu

Anmeldung unter: [https://www.kempodium.de/kurse/](https://www.kempodium.de/kurse/)

Wir freuen uns über euer Kommen :)

