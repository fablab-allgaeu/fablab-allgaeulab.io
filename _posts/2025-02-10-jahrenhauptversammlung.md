---
layout: post
title: Einladung Jahreshauptversammlung 2025
date:   2025-02-10
author: Jan
categories: Verein
---

##### **Liebe Mitglieder und Freunde des FabLab Allgäu e.V.,**  

wie jedes Jahr laden wir Euch herzlich zur Jahreshauptversammlung 2025 ein. 
Dieses mal findet die Versammlung in unserer neuen Werkstatt statt.

Natürlich sind auch Nicht-Mitglieder, Familienangehörige und Freunde willkommen!  

📅 **Datum:** Montag, 24. Februar 2025  
⏰ **Uhrzeit:** 20:00 Uhr  
📍 **Ort:** Jugendhaus, Landwehrstraße 2, 87439 Kempten (Allgäu)  

##### **Tagesordnung:**  
1. Begrüßung  
2. Jahresbericht  
3. Kassenbericht  
4. Bericht der Kassenprüfer  
5. Änderung der Gebührenordnung  
6. Wünsche und Anträge  

Wir freuen uns darauf, Dich bei der Jahreshauptversammlung begrüßen zu dürfen!

**Viele Grüße**  
Eure Vorstandschaft  
Jan, Matthias & André
