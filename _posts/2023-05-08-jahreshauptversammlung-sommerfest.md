---
layout: post
title:  "[Update] Jahreshauptversammlung und Sommerfest des FabLab Allgäu e.V. am 27. Mai 2023"
date:   2023-04-28
author: Jan
categories: Verein, Sommerfest
---

Liebe Mitglieder und Freunde des FabLab Allgäu e.V.,

wir freuen uns, euch zur diesjährigen Jahreshauptversammlung und unserem traditionellen Sommerfest einladen zu dürfen! Nach dem Erfolg des letzten Jahres, trotz Regen und Kälte, sind wir zuversichtlich, dass auch dieses Jahr wieder ein tolles Event auf uns wartet. Jeder ist herzlich eingeladen, an dieser Veranstaltung teilzunehmen - bringt gerne auch eure Familie und Freunde mit!

**Datum:** Samstag, 27. Mai 2023

**Zeit:** ab 14:00 Uhr

**Ort:** Jugendcampingplatz des Stadtjugendringes, Im Rotkreuz 33

Da das Wetter in der Vergangenheit unbeständig war, möchten wir euch darauf hinweisen, gutes Schuhwerk mitzubringen, um für alle Eventualitäten gerüstet zu sein.

Der Verein stellt die Infrastruktur und Getränke für das Fest bereit. Eine Anmeldung ist nicht zwingend notwendig, erleichtert uns jedoch die Planung. Grillgut, Salate und Brot müssen selbst mitgebracht werden.

Das Programm beginnt um 14:00 Uhr, gefolgt von der Jahreshauptversammlung (ohne Wahlen) von 15:00 bis 16:00 Uhr. Hier die Tagesordnung für die Jahreshauptversammlung:

    1. Begrüßung
    2. Jahresbericht
    3. Kassenbericht
    4. Bericht der Kassenprüfer
    5. Wünsche und Anträge

Anschließend wird gemeinsam gegrillt und in gemütlicher Runde gefeiert.

Wir freuen uns auf ein großartiges Sommerfest und eine erfolgreiche Jahreshauptversammlung mit euch!

Herzliche Grüße,

Eure Vorstandstandschaft

##### Update:

Wir bitten euch freundlich, euer eigenes Besteck, Geschirr und Gläser mitzubringen, da diese vor Ort nicht zur Verfügung stehen.

Außerdem würden wir uns über eure Anmeldungen freuen, um die Menge der Getränke besser einschätzen zu können.

Falls jemand noch eine Mitfahrgelegenheit vom Stadtzentrum zum Campingplatz benötigt, bitten wir darum, sich bei uns in der Telegramgruppe zu melden.

Selbstversändlich besteht keine Verpflichtung, den ganzen Tag vor Ort zu sein. Ihr könnt gerne auch erst nach der Jahreshauptversammlung zum gemeinsamen Grillen dazustoßen.

Angesichts der positiven Wettervorhersage empfehlen wir, statt der Gummistiefel, die letztes Jahr erforderlich waren, diesmal eher Sonnencreme mitzubringen.

##### Checkliste für das Sommerfest:

🍽️ Teller und Besteck

🍷 Gläser

🥩 Grillgut

🥗 (Salat)

🥖 Brot

🫙 Grill-Soße

👠 festes Schuhwerk

🧥 warme Klamotten für den Abend

🌞 Sonnencreme

