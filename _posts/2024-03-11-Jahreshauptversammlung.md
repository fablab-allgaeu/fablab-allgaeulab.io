---
layout: post
title: Einladung Jahreshauptversammlung 2024
date:   2024-03-11
author: Jan
categories: Verein
---
Liebe Mitglieder,

wie jedes Jahr laden wir Dich auch heuer zur Jahreshauptversammlung ein.

Natürlich sind auch Nicht-Mitglieder, Familie und Freunde herzlich willkommen.

Datum: Mittwoch, 17. April 2024

Uhrzeit: 20 Uhr

Ort: Seminarraum beim Stadtjugendring, Bäckerstraße 9, 87435 Kempten

Tagesordnung:
1. Begrüßung
2. Jahresbericht
3. Kassenbericht
4. Bericht der Kassenprüfer
5. Neuwahlen
5.1 1. Vorstand
5.2 2. Vorstand
5.3 Kassier
6. Wünsche und Anträge

Wir würden uns freuen, wenn wir Dich, bei dieser Jahreshauptversammlung begrüßen dürfen.

Grüße

Eure Vorstandschaft
