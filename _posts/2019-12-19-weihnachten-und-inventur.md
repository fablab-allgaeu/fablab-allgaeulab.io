---
layout: post
title:  "Weihnachten und Inventur - wieder da ab 15.01."
date:   2019-12-19
author: Bene
categories: openlab
---
Die nächsten 3 Wochen wird es kein OpenLab geben.

Während der Weihnachtsferien ist das Kempodium geschlossen. Das gleiche gilt dann natürlich auch für das FabLab.
In der 2. Januarwoche müssen wir dann eine Inventur durchführen, deshalb haben wir beschlossen auch am 08.01. kein OpenLab zu veranstalten.

Dafür sind wir wieder ab dem 15.01. da.

Wir wünschen euch eine besinnliche Weihnachtszeit und einen guten Rutsch ins neue Jahr!

