---
layout: post
title:  "🚀 Save the date - Sommerfest am Samstag 27. Mai 2023"
date:   2023-04-12
author: Jan
categories: Verein
---

Liebe Freunde des FabLab Allgäu,

wir möchten euch herzlich zu unserem diesjährigen Sommerfest einladen!

Save the Date: Das Sommerfest findet am 27. Mai 2023 statt.
Weitere Details werden bald folgen, also haltet euch den Termin frei.

Wir freuen uns darauf, mit euch zu feiern und den Sommer zu genießen!

Euere FabLab Alläu Vorstandschaft
