---
layout: post
title:  "Treffen des FabLab Allgäu e. V. am 21. Juli 2021"
date:   2021-07-16
author: Jan
categories: Verein Treffen
---

Liebe Freunde des FabLab Allgäu e. V.,

am Mittwoch, den 21. Juli 2021 wird sich das Fablab von 18-21 Uhr im [Künstlerhaus in Kempten](https://www.openstreetmap.org/?mlat=47.72436&mlon=10.31537#map=19/47.72436/10.31537&layers=N) treffen.

Ich hoffe das Wetter spielt mit und wir können im Biergarten sitzen.

Natürlich sind dazu alle Interessierten herzlich eingeladen!

Von Seiten der Vorstandschaft gibt es folgende Themen zu berichten:

1. Ergebnis Absprache mit dem Kulturamt der Stadt Kempten
    1. Räumlichkeiten
    1. Vernetzung mit anderen Vereinen
    1. Werbestand in der Fußgängerzone
1. Abschlussworkshop Kulturentwicklung Kempten
1. Aktueller Stand Allgäuhalle
1. Update von der Hochschule bezüglich Fablab
1. Idee: Öffentliches [Pixelflut](https://wiki.cccgoe.de/wiki/Pixelflut) Event im Künstler

Hauptsächlich soll aber natürlich der offene Austausch an diesem Abend im Vordergrund stehen.

Die Vorstandschaft freut sich auf euer Kommen!


<br>
<br>
Anfahrt Künstlerhaus Kempten:
<div id="mapKuenstler" class="map" style="height: 380px;"></div>
<script>
var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
		osmAttrib = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
		osm = L.tileLayer(osmUrl, {attribution: osmAttrib});

	var mapKuenstler = L.map('mapKuenstler', {zoom: 16, dragging: !L.Browser.mobile, scrollWheelZoom: false}).setView([47.72436, 10.31537], 16).addLayer(osm);

	L.marker([47.72436, 10.31537])
		.addTo(mapKuenstler)
		.bindPopup('Künstlerhaus Kempten<br>Beethovenstraße 2<br> 87435 Kempten')
		.openPopup();
</script>
