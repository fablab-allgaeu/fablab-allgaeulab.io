---
layout: post
title:  "Raspberry Pi Workshop"
date:   2015-09-23
---
Der Raspberry Pi ist in der Maker Szene seit einigen Jahren die Wahl schlechthin, wenn es darum geht, Projekte zu verwirklichen für die die Möglichkeiten eines kleinen Mikrocontrollers wie etwa des Arduino nicht mehr ausreichen. Gerade Aufgaben wie Netzwerkanbindung, Soundausgabe oder grafische Benutzeroberflächen sind auf einer leistungsfähigeren Hardware mit Linux Umgebung einfach realisierbar.

Wir bieten daher einen Einsteigerkurs für den Raspberry Pi an, in dessen Zuge die Teilnehmer lernen wie man einen Pi in Betrieb nimmt, Software unter Linux installiert und mit den Ein- und Ausgabemöglichkeiten (GPIO) des Pi's umgeht. Als Projekt hierfür haben wir uns ein Internetradio überlegt, mit dem man Radiostationen Weltweit anhören kann. 

Der Kurs ist in drei Abende aufgeteilt, je von 18 Uhr bis etwa 21 Uhr:  
**1. Abend Dienstag, 03.11.2015:** Einführung, Installation und Konfiguration  
**2. Abend Dienstag, 10.11.2015:** Ein-/Ausgänge (GPIO), Tasten und LEDs ansteuern  
**3. Abend Dienstag, 17.11.2015:** Aufbau des Internetradios mit dem erlernten Wissen

Die benötigte Hardware kann von uns bezogen werden. Wer selbst schon einen Pi hat, kann diesen mitbringen und spart sich die 40€ dafür.

**Die komplette Hardware (incl. Pi) kostet 80€ zzgl. zu den 90€ Kursgebühr.
ANMELDESCHLUSS ist der 13.10.2015.**

Wir freuen uns auf viele Teilnehmer!

