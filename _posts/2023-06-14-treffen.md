---
layout: post
title:  🐙 Monatliches Treffen des FabLab Allgäu e.V.
date:   2023-06-14
author: Jan
categories: Verein
---


Hallo liebe Mitglieder und Freunde des FabLab Allgäu,

wir freuen uns, euch zu unserem monatlichen Treffen einzuladen, das am 21. Juni 2023 um 20 Uhr stattfinden wird.

Wie immer ist dies eine großartige Gelegenheit, sich mit Gleichgesinnten auszutauschen, über spannende Projekte sich auszutauschen und neue Ideen zu entwickeln.

Dieses Mal haben wir folgende Punkte auf der Agenda:

1. Feedback Sommerfest
2. Update neue Werkstatt
3. Projektvorstellung TonUNIO
4. Diskussion Hackethon zur Umgestaltung der Webseite

Treffpunkt ist wie immer der Seminarraum beim Stadtjugendring in der Backerstraße 9.

Wir freuen uns auf eure Teilnahme und einen produktiven Austausch.

Bis bald,

Euer FabLab Allgäu Team
